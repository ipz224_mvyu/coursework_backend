<?php

/** @var string $error_message Повідомлення про помилку */
?>
<div class="wrapper-cont">
    <div class="wrapper">
        <form method="post" action="">
            <?php if (!empty($error_message)) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= $error_message ?>
                </div>
            <?php endif; ?>
            <h2>Авторизація</h2>
            <div class="input-box">
                <input name="login" type="text" id="loginInput" aria-describedby="loginHelp" placeholder="Email">
                <i class='bx bx-user'></i>
            </div>
            <div class="input-box">
                <input name="password" type="password" id="inputPassword" placeholder="Пароль">
                <i class='bx bxs-lock-alt'></i>
            </div>
            <button type="submit" class="btn">Увійти</button>

            <div class="register-link">
                <p>Немає аккаунту?<a href="/user/register">Завести профіль</a></p>
            </div>
        </form>
    </div>
</div>