<?php

/** @var string $error_message Повідомлення про помилку 
 */
$this->Title = 'Реєстрація';
?>

<div class="wrapper-cont">
    <div class="wrapper">
        <form method="post" action="">
            <?php if (!empty($error_message)) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= $error_message ?>
                </div>
            <?php endif; ?>
            <h2>Реєстрація</h2>
            <div class="input-box">
                <input name="login" type="text" id="loginInput" aria-describedby="loginHelp" placeholder="Логін" required value="<?= $this->controller->post->login ?? '' ?>">
                <i class='bx bx-user'></i>
            </div>
            <div class="input-box">
                <input name="password" type="password" id="inputPassword" placeholder="Пароль" required>
                <i class='bx bxs-lock-alt'></i>
            </div>
            <div class="input-box">
                <input name="confirm_password" type="password" id="confirmPassword" placeholder="Підтвердьте пароль" required>
                <i class='bx bxs-lock-alt'></i>
            </div>
            <div class="input-box">
                <input name="email" type="email" id="emailInput" placeholder="Пошта" required value="<?= $this->controller->post->email ?? '' ?>">
                <i class='bx bx-envelope'></i>
            </div>
            <div class="input-box">
                <input name="first_name" type="text" id="firstNameInput" placeholder="Ім'я" required value="<?= $this->controller->post->first_name ?? '' ?>">
                <i class='bx bx-user'></i>
            </div>
            <div class="input-box">
                <input name="last_name" type="text" id="lastNameInput" placeholder="Прізвище" required value="<?= $this->controller->post->last_name ?? '' ?>">
                <i class='bx bx-user'></i>
            </div>
            <div class="input-box">
                <input name="phone" type="text" id="phoneInput" placeholder="Телефон" required value="<?= $this->controller->post->phone ?? '' ?>">
                <i class='bx bx-phone'></i>
            </div>
            <div class="input-box">
                <input name="date_of_birth" type="date" id="dobInput" placeholder="Дата народження" required value="<?= $this->controller->post->date_of_birth ?? '' ?>">
                <i class='bx bx-calendar'></i>
            </div>
            <div class="input-box">
                <input name="street_and_number" type="text" id="streetInput" placeholder="Вулиця і номер" required value="<?= $this->controller->post->street_and_number ?? '' ?>">
                <i class='bx bx-home'></i>
            </div>
            <div class="input-box">
                <input name="additional_address_line1" type="text" id="additionalAddress1Input" placeholder="Додаткова адреса 1" value="<?= $this->controller->post->additional_address_line1 ?? '' ?>">
                <i class='bx bx-home'></i>
            </div>
            <div class="input-box">
                <input name="additional_address_line2" type="text" id="additionalAddress2Input" placeholder="Додаткова адреса 2" value="<?= $this->controller->post->additional_address_line2 ?? '' ?>">
                <i class='bx bx-home'></i>
            </div>
            <div class="input-box">
                <input name="zip_code" type="text" id="zipCodeInput" placeholder="Поштовий індекс" required value="<?= $this->controller->post->zip_code ?? '' ?>">
                <i class='bx bx-map'></i>
            </div>
            <div class="input-box">
                <input name="city" type="text" id="cityInput" placeholder="Місто" required value="<?= $this->controller->post->city ?? '' ?>">
                <i class='bx bx-building-house'></i>
            </div>
            <div class="input-box">
                <input name="country" type="text" id="countryInput" placeholder="Країна" required value="<?= $this->controller->post->country ?? '' ?>">
                <i class='bx bx-globe'></i>
            </div>
            <button type="submit" class="btn">Зареєструватися</button>
        </form>
    </div>
</div>

