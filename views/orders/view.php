<?php

use models\Users;
?>
<?php if (Users::IsUserLogged() && Users::GetAccess() === 4) : ?>
    <div class="container" style="margin-top: 20px;">
        <?php if (isset($orders) && is_array($orders)) : ?>
            <section id="orders-section">
                <div class="order-grid">
                    <?php foreach ($orders as $order) : ?>
                        <div class="order-item">
                            <div class="info-details">
                                <h3>Замовлення ID: <?= $order->id; ?></h3>
                                <p><strong>Телефон користувача:</strong> <?= $order->user_phone; ?></p>
                                <p><strong>Дата замовлення:</strong> <?= $order->order_date; ?></p>
                                <p><strong>Загальна ціна:</strong> <?= $order->total_price; ?>₴</p>
                                <p><strong>Адреса користувача:</strong> <?= $order->user_street_and_number; ?>, <?= $order->user_city; ?></p>
                                <a href="/orders/details/<?= $order->id; ?>" class="details-button">Детальна інформація</a>
                                <form class="delete-form" data-action="deleteOrders" method="POST">
                                    <input type="hidden" name="order_id" value="<?= $order->id; ?>">
                                    <button type="submit" class="delete-order-button">Видалити замовлення</button>
                                </form>
                            </div>
                            <div class="unique-id <?= $order->status ? 'completed-order' : ''; ?>"><?= $order->unique_id; ?></div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </section>
        <?php else : ?>
            <p class="no-orders">Немає доступних замовлень.</p>
        <?php endif; ?>
    </div>
<?php endif; ?>