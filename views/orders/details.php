<div class="container" style="margin-top: 20px;">
    <?php if (isset($products) && is_array($products)) : ?>
        <div class="product-details-order-grid">
            <?php foreach ($products as $item) : ?>
                <div class="product-details-order-item">
                    <a href="/products/show/<?= $item['product']->id; ?>" class="product-link">
                        <img src="../../src/img/<?= $item['product']->image; ?>" alt="<?= $item['product']->name; ?>" class="product-image">
                    </a>
                    <div class="product-info">
                        <h3><?= $item['product']->name; ?></h3>
                        <p><strong>Категорія:</strong> <?= $item['product']->category; ?></p>
                        <p><strong>Підкатегорія:</strong> <?= $item['product']->subcategory; ?></p>
                        <p><strong>Бренд:</strong> <?= $item['product']->brand; ?></p>
                        <p><strong>Кількість на складі:</strong> <?= $item['product']->stock_quantity; ?></p>
                        <p><strong>Кількість:</strong> <?= $item['quantity']; ?></p>
                        <p><strong>Ціна за кількість:</strong> <?= $item['subtotal']; ?>₴</p>
                    </div>
                    <form method="post" data-action="deleteDetail" class="delete-form">
                        <input type="hidden" name="orderDetailId" value="<?= $item['order_detail_id']; ?>">
                        <input type="hidden" name="order_id" value="<?= $item['orderId'] ?>">
                        <button type="submit" class="delete-button">Видалити</button>
                    </form>
                </div>
            <?php endforeach; ?>
        </div>

        <?php if ($status === 0) : ?>
            <a href="#" class="order-button" data-order-id="<?= $item['orderId'] ?>">Оформити замовлення</a>
        <?php endif; ?>
    <?php else : ?>
        <p>Немає деталей для цього замовлення.</p>
    <?php endif; ?>
</div>