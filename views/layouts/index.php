<?php

use models\Users;

if (empty($Title)) {
    $Title = '';
}
if (empty($Content)) {
    $Content = '';
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $Title ?></title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../../src/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Icons+Outlined">
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css" integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
    <header id="header-section">
        <div class="container container-header">
            <div class="header">
                <a href="/">
                    <img class="header__logo" src="../../src/icons/logosvg.svg" alt="Лого">
                </a>

                <nav class="nav-main">
                    <ul class="nav-main__list">
                        <li class="nav-main__item">
                            <a class="nav-main__link" href="/products/index">Товари</a>
                        </li>
                        <?php if (Users::IsUserLogged() && Users::GetAccess() === 4) : ?>
                            <li class="nav-main__item">
                                <a class="nav-main__link" href="/orders/view">Замовлення</a>
                            </li>
                        <?php endif; ?>
                        <?php if (Users::IsUserLogged() && Users::GetAccess() === 5) : ?>
                            <li class="nav-main__item">
                                <a class="nav-main__link" href="/products/addProduct">Новий товар</a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </nav>
                <div class="account-icons">
                    <div class="account-icon">
                        <a href="#"><i class="fa-solid fa-user"></i></a>
                        <span class="account-text">Мій аккаунт</span>
                        <ul class="account-dropdown">
                            <?php if (Users::IsUserLogged()) : ?>
                                <li><a href="http://cms/user/logout">Вийти</a></li>
                            <?php else : ?>
                                <li><a href="http://cms/user/login">Увійти</a></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <?php if (Users::IsUserLogged()) : ?>
                        <div class="account-icon">
                            <a href="/wishlist/getWishlistProducts" id="wishlist-link"><i class="fa-solid fa-heart"></i></a>
                            <span class="account-text">Список бажань</span>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="header-action">
                    <form action="/products/search" method="post">
                        <div class="search-container">
                            <input type="text" class="search-input" name="input" placeholder="Пошук товарів...">
                            <button type="submit" name="submit" class="search-button material-icons-outlined">search</button>
                        </div>
                    </form>
                    <button class="header-action__cart material-icons-outlined"><i class="fa-sharp fa-solid fa-cart-shopping" id="cart-icon"></i></button>
                </div>

            </div>
            <nav class="nav-categories">
                <ul class="categories-list">
                    <li>
                        <a href="/products/Maincategory/Одяг" class="menu-label">Одяг &bigtriangledown;</a>
                        <ul class="sub-menu">
                            <li><a href="/products/category/Футболки">Футболки та майки</a></li>
                            <li><a href="/products/category/Штани">Шорти та штани</a></li>
                            <li><a href="/products/category/Куртки">Куртки та вітровки</a></li>
                            <li><a href="/products/category/Комплекти">Комплекти для тренувань</a></li>
                            <li><a href="#">Спортивні костюми</a></li>
                            <li><a href="#">Легінси та тайтси</a></li>
                            <li><a href="#">Білизна</a></li>
                            <li><a href="#">Аксесуари (шапки, рукавички, шкарпетки)</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menu-label">Взуття &bigtriangledown;</a>
                        <ul class="sub-menu">
                            <li><a href="#">Бігове взуття</a></li>
                            <li><a href="#">Тренувальне взуття</a></li>
                            <li><a href="#">Футбольне взуття</a></li>
                            <li><a href="#">Баскетбольне взуття</a></li>
                            <li><a href="#">Взуття для тенісу</a></li>
                            <li><a href="#">Взуття для йоги та пілатесу</a></li>
                            <li><a href="#">Взуття для плавання</a></li>
                            <li><a href="#">Взуття для активного відпочинку (трекінг, гірські походи)</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="/products/Maincategory/Знаряддя_для_тренувань" class="menu-label">Знаряддя для тренувань &bigtriangledown;</a>
                        <ul class="sub-menu">
                            <li><a href="/products/category/Гантелі_та_гантельні_комплекти">Гантелі та гантельні комплекти</a></li>
                            <li><a href="#">Штанги та грифи</a></li>
                            <li><a href="#">Тренажери для силових тренувань</a></li>
                            <li><a href="#">М'ячі (футбольні, баскетбольні, волейбольні)</a></li>
                            <li><a href="#">Йога та пілатес (килими, блоки, ремені)</a></li>
                            <li><a href="#">Кардіо обладнання (бігові доріжки, велотренажери, орбітреки)</a></li>
                            <li><a href="#">Скакалки</a></li>
                            <li><a href="#">Еспандери та резинки</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menu-label">Спортивна екіпіровка &bigtriangledown;</a>
                        <ul class="sub-menu">
                            <li><a href="#">Шоломи та захисні елементи (для велосипедів, роликів, скейтбордів)</a></li>
                            <li><a href="#">Окуляри для плавання та спортивних занять</a></li>
                            <li><a href="#">Захист для суглобів (наколінники, налокітники, фіксатори)</a></li>
                            <li><a href="#">Рукавички (для тренувань, велосипедні, боксерські)</a></li>
                            <li><a href="#">Спортивні сумки та рюкзаки</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menu-label">Аксесуари &bigtriangledown;</a>
                        <ul class="sub-menu">
                            <li><a href="#">Пляшки для води</a></li>
                            <li><a href="#">Рушники</a></li>
                            <li><a href="#">Фітнес-трекери та годинники</a></li>
                            <li><a href="#">Підтримки для спини та корсети</a></li>
                            <li><a href="#">Пояси для фітнесу та важкої атлетики</a></li>
                            <li><a href="#">Сумки та рюкзаки</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="menu-label">Ігри та розваги &bigtriangledown;</a>
                        <ul class="sub-menu">
                            <li><a href="#">Спортивні ігри на відкритому повітрі (бадмінтон, фрісбі, бочче)</a></li>
                            <li><a href="#">Настільні ігри (настільний теніс, футбол, хокей)</a></li>
                            <li><a href="#">Водні види спорту (дошки для серфінгу, SUP, каяки)</a></li>
                            <li><a href="#">Ролики та скейтборди</a></li>
                            <li><a href="#">Вітрильні дошки</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="/products/Maincategory/Харчування" class="menu-label">Харчування та вітаміни &bigtriangledown;</a>
                        <ul class="sub-menu">
                            <li><a href="/products/category/Протеїни_та_гейнери">Протеїни та гейнери</a></li>
                            <li><a href="#">Вітамінні комплекси</a></li>
                            <li><a href="#">Енергетичні напої та батончики</a></li>
                            <li><a href="#">Спортивні добавки (креатин, амінокислоти, жироспалювачі)</a></li>
                            <li><a href="#">Гідратаційні засоби</a></li>
                            <li><a href="#">Веганські та органічні продукти</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="nav-container">
            <div class="cart">
                <h1 class="cart-title">Ваш Кошик</h1>
                <div class="cart-content">
                </div>
                <div class="total">
                    <div class="total-title">Загальна</div>
                    <div class="total-price">₴0</div>
                </div>
                <input type="text" class="styled-input" name="phone" placeholder="Введіть ваш номер телефону" maxlength="13">
                <p>Перед покупкою бажано зареєструйтесь</p>
                <button type="button" class="btn-buy">Купити</button>
                <i class='bx bx-x' id="close-cart"></i>
            </div>
        </div>
    </header>
    <div>
        <?= $Content ?>
    </div>
    <footer class="main-footer" id="footer-section" style="padding-top: 40px;">
        <div class="container">
            <div class="footer">
                <a href="/">
                    <img class="footer__img" src="../../src/icons/logosvg.svg" alt="№">
                </a>
                <ul class="footer__list">
                    <li class="footer__item">
                        <a class="footer__link" href="/">Головна</a>
                    </li>
                    <li class="footer__item">
                        <a class="footer__link" href="/products/index">Товари</a>
                    </li>
                </ul>
                <div class="social-icons">
                    <a href="#"><i class="fa-brands fa-instagram"></i></a>
                    <a href="#"><i class="fa-brands fa-twitter"></i></a>
                    <a href="#"><i class="fa-brands fa-facebook"></i></a>
                    <a href="#"><i class="fa-brands fa-youtube"></i></a>
                    <a href="#"><i class="fa-brands fa-telegram"></i></a>
                    <a href="#"><i class="fa-solid fa-envelope"></i></a>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <p class="footer-copyright__text">e-Tennis © 2021. All rights reserved.</p>
            </div>
        </div>
    </footer>
    <script defer src="../../src/js.js"></script>
    <script defer src="../../src/cart.js"></script>
    <script defer src="../../src/cookie.js"></script>
    <script defer src="../../src/wishlist.js"></script>
    <script defer src="../../src/order.js"></script>
    <script defer src="../../src/rating.js"></script>
</body>

</html>