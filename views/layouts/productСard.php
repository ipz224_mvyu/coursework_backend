<?php if (isset($products) && is_array($products)) : ?>
    <section id="products-section">
        <div class="container">
            <div class="product-grid">
                <?php foreach ($products as $product) : ?>
                    <div class="product-item">
                        <div class="product">
                            <div class="image">
                                <img src="../../src/img/<?= $product->image; ?>" alt="<?= $product->name; ?>">
                            </div>
                            <div class="info">
                                <h3><?= $product->name; ?></h3>
                                <div class="info-price">
                                    <?php if ($product->discount > 0) : ?>
                                        <span class="original-price"><?= $product->price; ?><small>₴</small></span>
                                    <?php endif; ?>
                                    <button class="add-to-card"><i class="fa-regular fa-heart"></i></button>
                                    <?php if ($product->discount > 0) : ?>
                                        <span class="discounted-price"><?= round($product->price * ((100 - $product->discount) / 100), 2); ?><small>₴</small></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php else : ?>
    <p>Немає доступних товарів.</p>
<?php endif; ?>




<?php
$page = $_GET['page'];
$count = 3;
?>

<?php if (isset($products) && is_array($products)) : ?>
    <section id="products-section">
        <div class="container">
            <div class="product-grid">
                <?php foreach ($products as $product) : ?>
                    <div class="product-item">
                        <div class="product">
                            <div class="image">
                                <img src="../../src/img/<?= $product->image; ?>" alt="<?= $product->name; ?>">
                            </div>
                            <div class="info">
                                <h3><?= $product->name; ?></h3>
                                <div class="info-price">
                                    <?php if ($product->discount > 0) : ?>
                                        <span class="original-price"><?= $product->price; ?><small>₴</small></span>
                                        <span class="discounted-price"><?= round($product->price * ((100 - $product->discount) / 100), 2); ?><small>₴</small></span>
                                    <?php else : ?>
                                        <span><?= $product->price; ?><small>₴</small></span>
                                    <?php endif; ?>
                                    <button class="add-to-card"><i class="fa-regular fa-heart"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php else : ?>
    <p>Немає доступних товарів.</p>
<?php endif; ?>

<div class="pagination">
    <a href="#">&NestedLessLess;</a>
    <a href="#" class="active"><button class="page_button"></button></a>
    <a href="#">2</a>
    <a href="#">3</a>
    <a href="#">4</a>
    <a href="#">5</a>
    <a href="#">6</a>
    <a href="#">7</a>
    <a href="#">8</a>
    <a href="#">&NestedGreaterGreater;;</a>
</div>