<section id="banner-section">
    <div class="container">
        <div class="banner">
            <div class="banner-info">
                <h2 class="banner__header text-on-ball">Знайди Своє Спортивне Екіпірування.</h2>
                <p class="banner__text text-on-ball">Ми пропонуємо найкращі товари для спорту, щоб допомогти тобі досягти нових висот. Якість, інновації та стиль - все це у нас.</p>
                <a href="products/index" class="banner__btn btn-primary text-on-ball">Переглянути Товари</a>
            </div>
        </div>
    </div>
</section>

<section id="sale-banner">
    <div class="container">
        <a class="sale-link" href="#products-section">
            <div class="sale-banner-content">
                <span class="sale-text"><b>Неймовірні знижки! Купуй зараз - плати менше!</b> 👇</span>
            </div>
        </a>
    </div>
</section>

<?php if (isset($products) && is_array($products)) : ?>
    <section id="products-section">
        <div class="container">
            <div class="product-grid">
                <?php foreach ($products as $product) : ?>
                    <div class="product-item">
                        <div class="product">
                            <div class="image">
                                <a href="/products/show/<?= $product->id; ?>">
                                    <img src="../../src/img/<?= $product->image; ?>" alt="<?= $product->name; ?>">
                                </a>
                            </div>
                            <div class="info">
                                <h3>
                                    <a href="/products/show/<?= $product->id; ?>">
                                        <?= $product->name; ?>
                                    </a>
                                </h3>
                                <div class="info-price">
                                    <?php if ($product->discount > 0) : ?>
                                        <span class="original-price"><?= $product->price; ?><small>₴</small></span>
                                        <span class="discounted-price"><?= number_format(round($product->price * ((100 - $product->discount) / 100), 2), 2, '.', ''); ?><small>₴</small></span>
                                    <?php else : ?>
                                        <span><?= $product->price; ?><small>₴</small></span>
                                    <?php endif; ?>
                                    <button id="add-to-wishlist-btn-<?= $product->id; ?>" data-product-id="<?= $product->id; ?>" type="button" class="add-to-wishlist" style="border: none; background: none;">
                                        <i class="fa-regular fa-heart"></i>
                                    </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php else : ?>
    <p>Немає доступних товарів.</p>
<?php endif; ?>


<section id="welcome-section">
    <div class="container">
        <div class="hero--text panel--body is--wide">
            <div class="teaser--text-long" style="display: block !important;">
                <div class="willkommen__title__headline-block">
                    <span class="hr-line hr-line--black hr-line--startpage">&nbsp;</span>
                    <h3 class="headline-block__title">ЛАСКАВО ПРОСИМО В FitSphere</h3>
                    <span class="hr-line hr-line--black hr-line--startpage">&nbsp;</span>
                </div>
                <div class="headline-block__small-title">Ваш інтернет-магазин для низьковартісних брендових товарів та спортивного обладнання</div>
                <h4 class="willkommen__title__title-block">Матеріали першого класу та інноваційні технології</h4>
                <div class="willkommen__title__text-block">Якість не обов'язково повинна бути дорогою! Будучи самі спортивними ентузіастами, ми знаємо про важливість високоякісного спортивного обладнання. Тому ми регулярно пропонуємо недорогий спортивний одяг від відомих виробників. Зручні шорти, дихаючі взуття, вітрозахисні костюми - ми маємо все це! І вам не доведеться обходитися без вашого улюбленого бренду. Ми пропонуємо продукцію таких відомих брендів, як adidas, Nike та PUMA, а також Reebok, Lonsdale та Dunlop. Наш асортимент величезний, і ми постійно додаємо нові продукти. Тож заходьте регулярно і не пропускайте наші сенсаційні пропозиції на недорогий спортивний одяг!</div>
                <h4 class="willkommen__title__title-block">Для футбольних фанатів і спортивних асів</h4>
                <div class="willkommen__title__text-block">Ваше серце б’ється за круглу шкіряну м’яч? Наше теж! В магазині футбольного одягу ви знайдете все, що ваше серце фаната може побажати. Окрім високоякісних м’ячів і футбольних черевиків для різних поверхонь, ми також пропонуємо багато, коли мова йде про одяг воротаря та <a title="обладнання для арбітрів" href="http://www.fitsphere.com/football/referee-equipment" target="_self"><strong>обладнання для арбітрів</strong></a>. Щоб виглядати так само чудово, як ви почуваєте себе під час тренувань, ми продаємо стильний <strong><a title="мода і спортивний одяг" href="http://www.fitsphere.com/fashion-sportswear" target="_self">мода і спортивний одяг</a></strong>, який зробить вас привабливими як за дощу, так і при ясній погоді. Дихаючі і міцні або просто вільні і круті - FitSphere пропонує недорогий спортивний одяг, який повністю відповідає вашим вимогам. Ви завжди будете виглядати чудово!</div>
                <h4 class="willkommen__title__title-block">Здобудьте свої переваги зараз!</h4>
                <div class="willkommen__title__text-block">FitSphere продає оригінальні продукти, що як нові, а також ліцензійний мерчандайз, такий як <a title="дешеві футбольні футболки" href="https://www.fitsphere.com/football/fanshop" target="_self"><strong>дешеві футбольні футболки</strong></a> за топовими цінами. Ми можемо досягати таких низьких цін, оскільки ми купуємо залишковий товар і припинені товари від виробників. Ми доставляємо ваші улюблені товари по всьому світу! <strong>Почніть зараз і заощаджуйте гроші!</strong></div>
            </div>
        </div>
    </div>
</section>