<div class="wrapper-cont">
    <div class="wrapper">
        <form method="post" action="" enctype="multipart/form-data" class="form">
            <?php if (!empty($error_message)) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= $error_message ?>
                </div>
            <?php endif; ?>
            <h2>Додавання продукту</h2>
            <div class="input-box">
                <input name="name" type="text" id="nameInput" placeholder="Назва продукту" required value="<?= $this->controller->post->name ?? '' ?>">
                <i class='bx bx-tag'></i>
            </div>
            <div class="input-box">
                <textarea name="description" id="descriptionInput" class="input-textarea" placeholder="Опис продукту" required><?= $this->controller->post->description ?? '' ?></textarea>
                <i class='bx bx-align-left'></i>
            </div>
            <div class="input-box">
                <select name="category" id="categoryInput" class="input-select" required>
                    <option value="">Виберіть категорію</option>
                    <option value="Одяг">Одяг</option>
                    <option value="Взуття">Взуття</option>
                    <option value="Знаряддя для тренувань">Знаряддя для тренувань</option>
                    <option value="Спортивна екіпіровка">Спортивна екіпіровка</option>
                    <option value="Аксесуари">Аксесуари</option>
                    <option value="Ігри та розваги">Ігри та розваги</option>
                    <option value="Харчування та вітаміни">Харчування та вітаміни</option>
                </select>
            </div>
            <div class="input-box">
                <select name="subcategory" id="subcategoryInput" class="input-select" required>
                    <option value="">Виберіть підкатегорію</option>
                    <option value="Футболки та майки">Футболки та майки</option>
                    <option value="Шорти та штани">Шорти та штани</option>
                    <option value="Куртки та вітровки">Куртки та вітровки</option>
                    <option value="Комплекти для тренувань">Комплекти для тренувань</option>
                    <option value="Спортивні костюми">Спортивні костюми</option>
                    <option value="Легінси та тайтси">Легінси та тайтси</option>
                    <option value="Білизна">Білизна</option>
                    <option value="Аксесуари (шапки, рукавички, шкарпетки)">Аксесуари (шапки, рукавички, шкарпетки)</option>
                    <option value="Кросівки">Кросівки</option>
                    <option value="Чоботи">Чоботи</option>
                    <option value="Шльопанці та босоніжки">Шльопанці та босоніжки</option>
                    <option value="Кеди та кросівки">Кеди та кросівки</option>
                    <option value="Спортивні рюкзаки">Спортивні рюкзаки</option>
                    <option value="Гантелі та штанги">Гантелі та штанги</option>
                    <option value="Еспандери">Еспандери</option>
                    <option value="Тренувальні мати">Тренувальні мати</option>
                    <option value="Боксерські рукавички">Боксерські рукавички</option>
                    <option value="Фітнес браслети">Фітнес браслети</option>
                    <option value="Ролики та скейти">Ролики та скейти</option>
                    <option value="Вітаміни та добавки">Вітаміни та добавки</option>
                    <option value="Спортивне харчування">Спортивне харчування</option>
                    <!-- інші підкатегорії -->
                </select>
            </div>
            <div class="input-box">
                <input name="brand" type="text" id="brandInput" placeholder="Бренд" required value="<?= $this->controller->post->brand ?? '' ?>">
                <i class='bx bx-building'></i>
            </div>
            <div class="input-box">
                <input name="price" type="number" step="0.01" id="priceInput" placeholder="Ціна" min="1" required value="<?= $this->controller->post->price ?? '' ?> ">
                <i class='bx bx-money'></i>
            </div>
            <div class="input-box">
                <input name="discount" type="number" id="discountInput" placeholder="Знижка (%)" value="<?= $this->controller->post->discount ?? '' ?>" min="0" max="100" step="1">
                <i class='bx bx-tag-alt'></i>
            </div>
            <div class="input-box">
                <input name="stock_quantity" type="number" min="1" id="stockQuantityInput" placeholder="Кількість на складі" required value="<?= $this->controller->post->stock_quantity ?? '' ?>">
                <i class='bx bx-package'></i>
            </div>
            <div class="input-box">
                <input name="color" type="text" id="colorInput" placeholder="Кольори (#ff0000, #00ff00)" value="<?= $this->controller->post->color ?? '' ?>">
                <i class='bx bx-paint'></i>
            </div>
            <div class="input-box">
                <input name="image" type="file" id="imageInput" required>
                <i class='bx bx-image'></i>
            </div>
            <button type="submit" class="btn">Додати продукт</button>
        </form>
    </div>
</div>