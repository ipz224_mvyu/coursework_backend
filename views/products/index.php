<?php

use models\Products;

if (isset($_POST['submit'])) {
    $input = $_POST['input'];
} else {
    $input = isset($_GET['input']) ? $_GET['input'] : '';
}

$current_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$page = null;
$parts = parse_url($current_url);
$query = [];

if (isset($parts['query'])) {
    parse_str($parts['query'], $query);
    if (isset($query['page'])) {
        $page = intval($query['page']);
        unset($query['page']); // Видаляємо параметр 'page', щоб уникнути дублювання
    }
    if (isset($query['input'])) {
        $input = $query['input']; // Зберігаємо параметр пошуку
    }
}

if ($page === null || $page <= 0) {
    $page = 1;
}
$count = 6;

if ($input === '') {
    $db_products = Products::findAll();
} else {
    $db_products = Products::findBySearch($input);
}

$products = [];
foreach ($db_products as $row) {
    $products[] = $row;
}

$total_products = count($products);
$page_count = ceil($total_products / $count);

$start_index = ($page - 1) * $count;
$products_page = array_slice($products, $start_index, $count);

// Функція для побудови URL з параметрами запиту
function build_url($base_url, $params)
{
    $query = http_build_query($params);
    return $base_url . '?' . $query;
}

?>

<?php if (!empty($products_page)) : ?>
    <section id="products-section">
        <div class="container">
            <div class="product-grid">
                <?php foreach ($products_page as $product) : ?>
                    <div class="product-item">
                        <div class="product">
                            <div class="image">
                                <a href="/products/show/<?= $product->id; ?>">
                                    <img src="../../src/img/<?= $product->image; ?>" alt="<?= $product->name; ?>">
                                </a>
                            </div>
                            <div class="info">
                                <h3>
                                    <a href="/products/show/<?= $product->id; ?>">
                                        <?= $product->name; ?>
                                    </a>
                                </h3>
                                <div class="info-price">
                                    <?php if ($product->discount > 0) : ?>
                                        <span class="original-price"><?= $product->price; ?><small>₴</small></span>
                                        <span class="discounted-price"><?= number_format(round($product->price * ((100 - $product->discount) / 100), 2), 2, '.', ''); ?><small>₴</small></span>
                                    <?php else : ?>
                                        <span><?= $product->price; ?><small>₴</small></span>
                                    <?php endif; ?>
                                    <button id="add-to-wishlist-btn-<?= $product->id; ?>" data-product-id="<?= $product->id; ?>" type="button" class="add-to-wishlist" style="border: none; background: none;">
                                        <i class="fa-regular fa-heart"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <div class="pagination-container">
        <div class="pagination">
            <?php if ($page > 1) : ?>
                <a href="<?= build_url($parts['path'], array_merge($query, ['page' => $page - 1, 'input' => $input])); ?>">&laquo;</a>
            <?php endif; ?>
            <?php for ($p = 1; $p <= $page_count; $p++) : ?>
                <a href="<?= build_url($parts['path'], array_merge($query, ['page' => $p, 'input' => $input])); ?>" class="<?= $p === $page ? 'active' : ''; ?>"><?= $p; ?></a>
            <?php endfor; ?>
            <?php if ($page < $page_count) : ?>
                <a href="<?= build_url($parts['path'], array_merge($query, ['page' => $page + 1, 'input' => $input])); ?>">&raquo;</a>
            <?php endif; ?>
        </div>
    </div>
<?php else : ?>
    <div class="container">
        <p>Немає доступних товарів.</p>
    </div>
<?php endif; ?>