<?php

use models\Users;

?>
<?php if (isset($product)) : ?>
    <section id="good-details" class="good-details" data-product-id="<?= $product->id; ?>">
        <div class="good-main">
            <div class="good-container">
                <div class="good-item">
                    <div class="good-content">
                        <div class="good-image">
                            <img id="good-image" src="../../src/img/<?= $product->image; ?>" alt="<?= $product->name; ?>">
                        </div>
                        <div class="good-text">
                            <h2><?= $product->name; ?></h2>
                            <p><?= $product->description; ?></p>
                            <?php if ($product->discount > 0) : ?>
                                <div class="price">
                                    <span class="discounted-price"><?= number_format(round($product->price * ((100 - $product->discount) / 100), 2), 2, '.', ''); ?><small>₴</small></span>
                                    <span class="original-price"><?= $product->price; ?><small>₴</small></span>
                                </div>
                            <?php else : ?>
                                <div class="price">
                                    <span><?= $product->price; ?><small>₴</small></span>
                                </div>
                            <?php endif; ?>

                            <div class="product-details">
                                <p><strong>Категорія: </strong> <?= $product->category; ?></p>
                                <p><strong>Підкатегорія: </strong> <?= $product->subcategory; ?></p>
                                <p><strong>Бренд: </strong> <?= $product->brand; ?></p>
                                <?php if (!empty($product->color)) : ?>
                                    <div class="color-squares">
                                        <strong>Кольори:</strong>
                                        <?php
                                        $colorsArray = explode(',', $product->color);
                                        foreach ($colorsArray as $color) {
                                            echo '<div class="color-square" style="background-color: ' . htmlspecialchars(trim($color)) . ';"></div>';
                                        }
                                        ?>
                                    </div>
                                <?php endif; ?>
                                <div class="availability-block">
                                    <?php if ($product->stock_quantity <= 0) : ?>
                                        <div class="availability-status unavailable">
                                            <i class="fas fa-times"></i> Немає в наявності
                                        </div>
                                    <?php else : ?>
                                        <div class="availability-status available">
                                            <i class="fas fa-check"></i> Є в наявності
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="rating">
                                    <div class="stars big-stars" style="--rating: <?= round($averageRating / 5 * 100, 2); ?>%;" data-product-id="<?= $product->id; ?>">
                                        <?php for ($i = 1; $i <= 5; $i++) : ?>
                                            <span class="star <?= ($i <= $averageRating) ? 'filled' : ''; ?>" data-rating="<?= $i; ?>">
                                                <i class="fas fa-star"></i>
                                            </span>
                                        <?php endfor; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="purchase-section">
                                <?php if (Users::IsUserLogged() && Users::GetAccess() === 5) : ?>
                                    <div class="input-group">
                                        <input type="number" class="price-input" placeholder="Нова ціна" step="0.01" min="0" required value="<?= $product->price; ?>">
                                        <button class="update-btn" data-action="/products/updatePrice" data-product-id="<?= $product->id; ?>">Оновити ціну</button>
                                    </div>
                                    <div class="input-group">
                                        <input type="number" class="discount-input" placeholder="Знижка (%)" min="0" max="100" required value="<?= $product->discount; ?>">
                                        <button class="update-btn" data-action="/products/updateDiscount" data-product-id="<?= $product->id; ?>">Оновити знижку</button>
                                    </div>
                                    <form method="post" action="/products/deleteProduct" class="form-delete-product">
                                        <input type="hidden" name="product_id" value="<?= $product->id; ?>">
                                        <button type="submit" name="delete" class="delete-btn" onclick="return confirm('Ви впевнені, що хочете видалити цей продукт?');">Видалити</button>
                                    </form>

                                <?php endif; ?>
                                <?php if ($product->stock_quantity > 0) : ?>
                                    <a href="#" class="purchase-btn" data-product-id="<?= $product->id; ?>">Зробити покупку</a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container_input">
                <?php if (Users::IsUserLogged()) : ?>
                    <div class="add-comment">
                        <textarea id="comment-description" rows="4" placeholder="Введіть ваш коментар"></textarea>
                        <button class="add-comment-btn" data-product-id="<?= $product->id; ?>">Додати коментар</button>
                    </div>
                <?php endif; ?>
            </div>
            <section class="comments-section">
                <h3>Коментарі</h3>
                <div id="comments-container">
                    <?php if (!empty($comments)) : ?>
                        <?php foreach ($comments as $comment) : ?>
                            <div class="comment" data-comment-id="<?= $comment->id; ?>">
                                <p><strong>Email: </strong><?= htmlspecialchars($comment->email); ?></p>
                                <p><?= htmlspecialchars($comment->description); ?></p>
                                <p><span class="stars"><?php echo generateStarsHtml($comment->rating); ?></span></p>
                                <p><strong>Дата: </strong><?= htmlspecialchars($comment->date); ?></p>
                                <?php if (Users::IsUserLogged() && $comment->user_id == Users::GetCurrentUser()->id || Users::GetAccess() === 5) : ?>
                                    <button class="delete-comment-btn" data-comment-id="<?= $comment->id; ?>" data-product-id="<?= $product->id; ?>">Видалити</button>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <p>На жаль, коментарів ще немає.</p>
                    <?php endif; ?>
                </div>
            </section>
        </div>

    </section>

<?php else : ?>
    <p>Товар не знайдено.</p>
<?php endif; ?>

<?php
function generateStarsHtml($rating)
{
    $starsHtml = '';
    $fullStar = '<span class="star-comments">&#9733;</span>';
    $emptyStar = '<span class="star-comments empty">&#9734;</span>';

    for ($i = 0; $i < 5; $i++) {
        if ($i < $rating) {
            $starsHtml .= $fullStar;
        } else {
            $starsHtml .= $emptyStar;
        }
    }

    return $starsHtml;
}
?>