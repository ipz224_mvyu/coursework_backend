<div class="cont404">
    <nav class="nav404">
        <div class="nav__logo"><a href="#">Web Design Mastery</a></div>
    </nav>
    <div class="container404">
        <div class="header404">
            <h1>404</h1>
            <h3>Сторінка не знайдена!</h3>
        </div>
        <img src="../../src/img/404.png" alt="not found" />
        <div class="footer404">
            <p>
                Вибачте, сторінку, яку ви запитували, не вдалося знайти. Будь ласка, поверніться на головну сторінку!
            </p>
            <a href="/">
                <button>На головну</button>
            </a>
        </div>
    </div>
</div>
<div class="spacer"></div>