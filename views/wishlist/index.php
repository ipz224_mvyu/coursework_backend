<?php if (isset($products) && is_array($products)) : ?>
    <section id="products-section">
        <div class="container">
            <div class="product-grid">
                <?php foreach ($products as $product) : ?>
                    <div class="product-item">
                        <div class="product">
                            <div class="image">
                                <a href="/products/show/<?= $product->id; ?>">
                                    <img src="../../src/img/<?= $product->image; ?>" alt="<?= $product->name; ?>">
                                </a>
                            </div>
                            <div class="info">
                                <h3>
                                    <a href="/products/show/<?= $product->id; ?>">
                                        <?= $product->name; ?>
                                    </a>
                                </h3>
                                <div class="info-price">
                                    <?php if ($product->discount > 0) : ?>
                                        <span class="original-price"><?= $product->price; ?><small>₴</small></span>
                                        <span class="discounted-price"><?= number_format(round($product->price * ((100 - $product->discount) / 100), 2), 2, '.', ''); ?><small>₴</small></span>
                                    <?php else : ?>
                                        <span><?= $product->price; ?><small>₴</small></span>
                                    <?php endif; ?>
                                    <button id="add-to-wishlist-btn-<?= $product->id; ?>" data-product-id="<?= $product->id; ?>" type="button" class="add-to-wishlist" style="border: none; background: none;">
                                        <i class="fa-regular fa-heart"></i>
                                    </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php else : ?>
    <p>Немає доступних товарів.</p>
<?php endif; ?>