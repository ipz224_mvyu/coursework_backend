document.querySelectorAll('.purchase-btn').forEach(function (button) {
	button.addEventListener('click', function (event) {
		event.preventDefault()
		var productId = this.getAttribute('data-product-id')
		var cartItems = getCookie('cart_items')

		var cartItemsArray = cartItems ? cartItems.split(';').filter(Boolean) : []

		if (!cartItemsArray.includes(productId)) {
			cartItemsArray.push(productId)
		}

		cartItems = cartItemsArray.join(';')

		createCookie('cart_items', cartItems, 5)

		showSuccessMessage('Товар успішно додано до кошика!')

		setTimeout(function () {
			window.location.reload()
		}, 1000)
	})
})

function createCookie(name, value, days) {
	var expires
	if (days) {
		var date = new Date()
		date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000)
		expires = '; expires=' + date.toUTCString()
	} else {
		expires = ''
	}
	var cookieString =
		name + '=' + encodeURIComponent(value) + expires + '; path=/'
	document.cookie = cookieString
}

function getCookie(name) {
	var nameEquals = name + '='
	var cookies = document.cookie.split(';')
	for (var i = 0; i < cookies.length; i++) {
		var cookie = cookies[i].trim()
		if (cookie.indexOf(nameEquals) === 0) {
			return decodeURIComponent(
				cookie.substring(nameEquals.length, cookie.length)
			)
		}
	}
	return null
}

function showSuccessMessage(message) {
	var successMessage = document.createElement('div')
	successMessage.className = 'success-message'
	successMessage.textContent = message
	document.body.appendChild(successMessage)

	setTimeout(function () {
		document.body.removeChild(successMessage)
	}, 3000)
}
