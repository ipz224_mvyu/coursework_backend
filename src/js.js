document.querySelectorAll('.add-comment-btn').forEach(function (button) {
	button.addEventListener('click', function () {
		var productId = this.getAttribute('data-product-id')
		var description = document.getElementById('comment-description').value

		fetch('/products/addComment', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				product_id: productId,
				description: description,
			}),
		})
			.then(response => {
				if (!response.ok) {
					throw new Error('Network response was not ok ' + response.statusText)
				}
				return response.json()
			})
			.then(data => {
				if (data.success) {
					console.log('Comment added successfully')
					var commentsContainer = document.getElementById('comments-container')
					commentsContainer.innerHTML = ''

					data.comments.forEach(function (comment) {
						var starsHtml = getStarsHtml(comment.rating)

						commentsContainer.innerHTML += `
                        <div class="comment" data-comment-id="${comment.id}">
                            <p><strong>Email: </strong>${comment.user_email}</p>
                            <p>${comment.description}</p>
                            <span class="stars">${starsHtml}</span>
                            <p><strong>Дата: </strong>${new Date(comment.created_at).toLocaleString()}</p>
                            <button class="delete-comment-btn" data-comment-id="${comment.id}" data-product-id="${productId}">Видалити</button>
                        </div>
                    `
					})

					document.getElementById('comment-description').value = ''
					attachDeleteCommentEvents()
				} else {
					console.error(data.error)
				}
			})
			.catch(error => {
				console.error('There was a problem with the fetch operation:', error)
			})
	})
})

function attachDeleteCommentEvents() {
	document.querySelectorAll('.delete-comment-btn').forEach(function (button) {
		button.addEventListener('click', function () {
			var commentId = this.getAttribute('data-comment-id')
			var productId = this.getAttribute('data-product-id')

			fetch('/products/deleteComment', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					comment_id: commentId,
					product_id: productId,
				}),
			})
				.then(response => {
					if (!response.ok) {
						throw new Error(
							'Network response was not ok ' + response.statusText
						)
					}
					return response.json().catch(() => {
						throw new Error('Invalid JSON response')
					})
				})
				.then(data => {
					if (data.success) {
						console.log('Comment deleted successfully')
						var commentsContainer =
							document.getElementById('comments-container')
						commentsContainer.innerHTML = ''

						if (data.comments.length > 0) {
							data.comments.forEach(function (comment) {
								var starsHtml = getStarsHtml(comment.rating)

								commentsContainer.innerHTML += `
                                <div class="comment" data-comment-id="${comment.id}">
                                    <p><strong>Email: </strong>${comment.user_email}</p>
                                    <p>${comment.description}</p>
                                    <span class="stars">${starsHtml}</span>
                                    <p><strong>Дата: </strong>${new Date(comment.created_at).toLocaleString()}</p>
                                    <button class="delete-comment-btn" data-comment-id="${comment.id}" data-product-id="${productId}">Видалити</button>
                                </div>
                            `
							})
						} else {
							commentsContainer.innerHTML = '<p>Немає коментарів</p>'
						}

						attachDeleteCommentEvents()
					} else {
						console.error(data.error)
					}
				})
				.catch(error => {
					console.error('There was a problem with the fetch operation:', error)
				})
		})
	})
}

function getStarsHtml(rating) {
	var starsHtml = ''
	var fullStar = '<span class="star-comments">&#9733;</span>'
	var emptyStar = '<span class="star-comments empty">&#9734;</span>'

	for (var i = 0; i < 5; i++) {
		if (i < rating) {
			starsHtml += fullStar
		} else {
			starsHtml += emptyStar
		}
	}

	return starsHtml
}

attachDeleteCommentEvents()

document.querySelectorAll('.update-btn').forEach(function (button) {
	button.addEventListener('click', function () {
		var action = this.getAttribute('data-action')
		var productId = this.getAttribute('data-product-id')
		var input = this.previousElementSibling
		var value = input.value

		fetch(action, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				product_id: productId,
				price: value,
			}),
		})
			.then(response => {
				if (!response.ok) {
					throw new Error('Network response was not ok ' + response.statusText)
				}
				return response.json()
			})
			.then(data => {
				if (data.success) {
					console.log('Price updated successfully')
				} else {
					console.error(data.error)
				}
			})
			.catch(error => {
				console.error('There was a problem with the fetch operation:', error)
			})
	})
})

document.querySelectorAll('.update-btn').forEach(function (button) {
	button.addEventListener('click', function () {
		var action = this.getAttribute('data-action')
		var productId = this.getAttribute('data-product-id')
		var input = this.previousElementSibling
		var value = input.value

		fetch(action, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				product_id: productId,
				discount: value, // Змінено поле з price на discount
			}),
		})
			.then(response => {
				if (!response.ok) {
					throw new Error('Network response was not ok ' + response.statusText)
				}
				return response.json()
			})
			.then(data => {
				if (data.success) {
					console.log('Discount updated successfully')
				} else {
					console.error(data.error)
				}
			})
			.catch(error => {
				console.error('There was a problem with the fetch operation:', error)
			})
	})
})
