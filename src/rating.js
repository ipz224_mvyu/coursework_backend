document.querySelectorAll('.stars .star').forEach(function (starElement) {
	starElement.addEventListener('click', function () {
		var ratingValue = this.getAttribute('data-rating')
		var productId = this.parentElement.getAttribute('data-product-id')

		if (!ratingValue || !productId) {
			console.error('Rating value or product ID is missing.')
			return
		}

		fetch('/products/ratingUpdate', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				product_id: productId,
				rating: ratingValue,
			}),
		})
			.then(response => {
				if (!response.ok) {
					throw new Error('Network response was not ok ' + response.statusText)
				}
				return response.json()
			})
			.then(data => {
				if (data.average_rating !== undefined) {
					var averageRating = data.average_rating
					updateStarDisplay(productId, averageRating)
				} else {
					console.error(data.error)
				}
			})
			.catch(error => {
				console.error('There was a problem with the fetch operation:', error)
			})
	})
})

function updateStarDisplay(productId, averageRating) {
	var starsElement = document.querySelector(
		'.stars[data-product-id="' + productId + '"]'
	)
	starsElement.style.setProperty('--rating', (averageRating / 5) * 100 + '%')

	starsElement.querySelectorAll('.star').forEach(function (starElement, index) {
		if (index < Math.floor(averageRating)) {
			starElement.classList.add('filled')
		} else {
			starElement.classList.remove('filled')
		}
	})
}


