document.addEventListener('DOMContentLoaded', function () {
	// Order button click handler
	var orderButton = document.querySelector('.order-button')
	if (orderButton) {
		orderButton.addEventListener('click', function (event) {
			event.preventDefault()

			var orderId = orderButton.dataset.orderId
			var formData = new FormData()
			formData.append('order_id', orderId)

			fetch('/orders/checkout', {
				method: 'POST',
				body: formData,
			})
				.then(response => {
					if (!response.ok) {
						throw new Error('Network response was not ok.')
					}
					return response.json()
				})
				.then(data => {
					if (data.error) {
						alert(data.error)
					} else {
						alert('Замовлення успішно оформлено.')
						window.location.reload()
					}
				})
				.catch(error => {
					alert('Помилка оформлення замовлення.')
				})
		})
	}

	// Delete form submit handler
	var deleteForms = document.querySelectorAll('.delete-form')

	deleteForms.forEach(function (form) {
		form.addEventListener('submit', function (event) {
			event.preventDefault()

			var formData = new FormData(form)
			var action = form.getAttribute('data-action')
			var fetchUrl = ''

			if (action === 'deleteOrders') {
				fetchUrl = '/orders/deleteOrders'
			} else if (action === 'deleteDetail') {
				fetchUrl = '/orders/deleteDetail'
			} else {
				alert('Невідома дія форми.')
				return
			}

			fetch(fetchUrl, {
				method: 'POST',
				body: formData,
			})
				.then(response => {
					if (!response.ok) {
						throw new Error('HTTP error, status = ' + response.status)
					}
					if (action === 'deleteOrders') {
						return response.json()
					} else if (action === 'deleteDetail') {
						return response.text()
					}
				})
				.then(data => {
					if (action === 'deleteOrders') {
						if (data.message) {
							form.closest('.order-item').remove()
							alert(data.message)
						} else if (data.error) {
							alert('Помилка видалення замовлення: ' + data.error)
						} else {
							throw new Error('Некоректний формат відповіді від сервера.')
						}
					} else if (action === 'deleteDetail') {
						try {
							const jsonData = JSON.parse(data)
							if (jsonData.redirect) {
								window.location.href = jsonData.redirect
							} else {
								form.closest('.product-details-order-item').remove()
								alert(jsonData.message)
							}
						} catch (error) {
							alert('Некоректний формат відповіді від сервера.')
						}
					}
				})
				.catch(error => {
					if (action === 'deleteOrders') {
						alert('Помилка видалення замовлення.')
					} else if (action === 'deleteDetail') {
						alert('Помилка видалення деталі замовлення.')
					}
				})
		})
	})
})
