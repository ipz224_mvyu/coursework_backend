function getProductsByIds(ids) {
	return fetch('/products/get', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({ ids: ids.join(';') }),
	})
		.then(response => {
			if (!response.ok) {
				throw new Error('Відповідь мережі не вдалась')
			}
			return response.json()
		})
		.then(products => {
			console.log('Отримані товари з бази даних:', products)
			return products
		})
		.catch(error => {
			console.error('Помилка при отриманні товарів:', error)
		})
}

function createOrder(phone, total) {
	console.log('Функція createOrder викликана з параметрами:', phone, total)

	var cartBoxes = document.querySelectorAll('.cart-box')
	var orderDetails = []

	cartBoxes.forEach(cartBox => {
		var productId = cartBox.dataset.productId

		var quantityElement = cartBox.querySelector('.cart-quantity')
		var priceElement = cartBox.querySelector('.cart-price')

		var quantity = parseInt(quantityElement.value)
		var price = parseFloat(priceElement.innerText.replace('₴', ''))
		var subtotal = quantity * price

		orderDetails.push({
			product_id: productId,
			quantity: quantity,
			subtotal: subtotal,
		})
	})

	console.log('Створення замовлення з параметрами:', phone, total, orderDetails)

	fetch('/orders/create', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			phone: phone,
			total_price: parseFloat(total),
			order_details: orderDetails,
		}),
	})
		.then(response => {
			console.log('Відповідь від сервера отримана', response)

			if (!response.ok) {
				console.log(
					'Помилка в відповіді сервера:',
					response.status,
					response.statusText
				)
				return response.text().then(text => {
					throw new Error('Сталась помилка при створенні замовлення: ' + text)
				})
			}

			console.log('Відповідь сервера в порядку, конвертуємо в JSON')
			return response.json()
		})
		.then(data => {
			console.log('Отримана відповідь від сервера:', data)
			if (data && data.message) {
				console.log('Замовлення створено успішно:', data.message)
				alert('Ваше замовлення прийнято!')
				clearCart()
			} else if (data && data.error) {
				console.error('Помилка при створенні замовлення:', data.error)
				alert('Сталась помилка при створенні замовлення: ' + data.error)
			} else {
				throw new Error('Отримано невірну відповідь від сервера.')
			}
		})
		.catch(error => {
			console.error('Помилка при створенні замовлення:', error)
			alert('Сталась помилка при створенні замовлення: ' + error.message)
		})
}

function getCookieValue(cookieName) {
	console.log("Пошук куки з ім'ям:", cookieName)
	var name = cookieName + '='
	var decodedCookie = document.cookie.split(';')
	var cookieArray = decodedCookie.map(item => item.trim())

	for (var i = 0; i < cookieArray.length; i++) {
		var cookie = cookieArray[i]
		if (cookie.indexOf(name) === 0) {
			var cookieValue = cookie.substring(name.length, cookie.length)
			console.log('Значення куки:', cookieValue)

			cookieValue = decodeURIComponent(cookieValue)
			console.log('Розкодоване значення куки:', cookieValue)

			var ids = cookieValue.split(';')
			console.log('Масив ID:', ids)
			return ids.filter(Boolean) 
		}
	}
	return []
}

function displayCartItems() {
	var cartItemsIds = getCookieValue('cart_items')
	console.log('ID товарів у кошику:', cartItemsIds)

	if (cartItemsIds.length === 0) {
		console.log('Кошик порожній')
		return
	}

	getProductsByIds(cartItemsIds)
		.then(products => {
			console.log('Отримані товари з бази даних:', products)

			var cartContent = document.querySelector('.cart-content')
			cartContent.innerHTML = '' 

			products.forEach(product => {
				var price = product.discount
					? ((product.price * (100 - product.discount)) / 100).toFixed(2)
					: product.price.toFixed(2)
				var cartBox = document.createElement('div')
				cartBox.classList.add('cart-box')
				cartBox.dataset.productId = product.id

				var cartBoxContent = `
				<img src="../../src/img/${product.image}" alt="" class="cart-img" data-product-id="${product.id}">
				<div class="detali-box">
					<div class="cart-product-title">${product.name}</div>
					<div class="cart-price">₴${price}</div>
					<input type="number" value="1" class="cart-quantity" min="1" max="20">
				</div>
				<i class='bx bxs-trash-alt cart-remove'></i>`
				cartBox.innerHTML = cartBoxContent

				cartBox
					.querySelector('.cart-remove')
					.addEventListener('click', removeCartItem)
				cartBox
					.querySelector('.cart-quantity')
					.addEventListener('change', quantityChanged)

				cartContent.appendChild(cartBox)
			})

			updateTotal()
		})
		.catch(error => {
			console.error('Помилка при отриманні товарів з бази даних:', error)
		})
}

function updateTotal() {
	var cartBoxes = document.querySelectorAll('.cart-box')
	var total = 0
	cartBoxes.forEach(cartBox => {
		var priceElement = cartBox.querySelector('.cart-price')
		var quantityElement = cartBox.querySelector('.cart-quantity')
		var price = parseFloat(priceElement.innerText.replace('₴', ''))
		var quantity = parseInt(quantityElement.value)
		total += price * quantity
	})
	total = Math.round(total * 100) / 100
	document.querySelector('.total-price').innerText = '₴' + total
}

function removeCartItem(event) {
	var buttonClicked = event.target
	var cartItem = buttonClicked.parentElement
	var productId = cartItem.dataset.productId 

	cartItem.remove() 

	var cartItemsIds = getCookieValue('cart_items')
	if (cartItemsIds.length === 0) {
		return
	}

	var index = cartItemsIds.indexOf(productId)
	console.log(index)
	if (index !== -1) {
		cartItemsIds.splice(index, 1)
	}
	console.log(encodeURIComponent(cartItemsIds.join(';')))
	createCookie('cart_items', cartItemsIds.join(';'), 5)

	updateTotal()
}


function quantityChanged(event) {
	var input = event.target
	if (isNaN(input.value) || input.value <= 0) {
		input.value = 1
	} else if (input.value > 20) {
		input.value = 20
	}
	updateTotal()
}

document.addEventListener('DOMContentLoaded', displayCartItems)

console.log('Скрипт кошика успішно завантажено.')

var cartIcon = document.querySelector('#cart-icon')
var cart = document.querySelector('.cart')
var closeCart = document.querySelector('#close-cart')

cartIcon.onclick = () => {
	cart.classList.add('active')
}

closeCart.onclick = () => {
	cart.classList.remove('active')
}

function ready() {
	var removeCartButtons = document.getElementsByClassName('cart-remove')
	for (var i = 0; i < removeCartButtons.length; i++) {
		var button = removeCartButtons[i]
		button.addEventListener('click', removeCartItem)
	}

	var quantityInputs = document.getElementsByClassName('cart-quantity')
	for (var i = 0; i < quantityInputs.length; i++) {
		var input = quantityInputs[i]
		input.addEventListener('change', quantityChanged)
	}

	var addCartButtons = document.getElementsByClassName('add-cart')
	for (var i = 0; i < addCartButtons.length; i++) {
		var button = addCartButtons[i]
		button.addEventListener('click', addCartClicked)
	}

	document
		.getElementsByClassName('btn-buy')[0]
		.addEventListener('click', buyButtonClicked)
}

function validatePhoneNumber(phoneNumber) {
	var phoneRegex = /^\+380\d{9}$/
	return phoneRegex.test(phoneNumber)
}

function buyButtonClicked() {
	var phoneNumber = document.querySelector('.styled-input').value
	var totalPrice = document
		.querySelector('.total-price')
		.innerText.replace('₴', '')

	if (!phoneNumber) {
		alert('Будь ласка, введіть ваш номер телефону.')
		return
	}

	if (!validatePhoneNumber(phoneNumber)) {
		alert('Будь ласка, введіть дійсний номер телефону у форматі +380XXXXXXXXX.')
		return
	}

	console.log(
		'Натиск кнопки Купити. Номер телефону:',
		phoneNumber,
		'Загальна сума:',
		totalPrice
	)
	createOrder(phoneNumber, totalPrice)
}

function clearCart() {
	var cartContent = document.querySelector('.cart-content')
	while (cartContent.hasChildNodes()) {
		cartContent.removeChild(cartContent.firstChild)
	}
	updateTotal()

	createCookie('cart_items', '', -1)
}

if (document.readyState == 'loading') {
	document.addEventListener('DOMContentLoaded', ready)
} else {
	ready()
}
