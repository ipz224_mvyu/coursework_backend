document.addEventListener('DOMContentLoaded', function () {
	const wishlistButtons = document.querySelectorAll('.add-to-wishlist')

	wishlistButtons.forEach(button => {
		button.addEventListener('click', function (e) {
			e.preventDefault()

			const productId = this.dataset.productId
			console.log('Product ID:', productId) // Вивід ID товару в консоль

			fetch('/wishlist/addToWishlist', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({ product_id: productId }),
			})
				.then(response => {
					console.log('Response status:', response.status) // Вивід статусу відповіді
					if (!response.ok) {
						throw new Error('Network response was not ok')
					}
					return response.json()
				})
				.then(data => {
					console.log('Response data:', data) // Вивід даних в консоль
					alert(data.message)
					if (data.status === 'success') {
						// Додаткові дії при успішному додаванні (необов'язково)
					}
				})
				.catch(error => {
					console.error('Fetch error:', error) // Вивід деталей помилки в консоль
				})
		})
	})
})

