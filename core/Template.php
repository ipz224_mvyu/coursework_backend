<?php

namespace core;

class Template
{
    protected $templateFilePath;
    protected $paramsArray;
    public Controller $controller;

    public function __set($name, $value)
    {
        Core::getInstance()->template->setParam($name, $value);
    }

    public function __construct($templateFilePath)
    {
        $this->templateFilePath = $templateFilePath;
        $this->paramsArray = [];
    }

    public function setTemplateFilePath($path)
    {
        $this->templateFilePath = $path;
    }

    public function setParam($paramName, $paramValue)
    {
        $this->paramsArray[$paramName] = $paramValue;
    }

    public function setParams($params)
    {
        $this->paramsArray = array_merge($this->paramsArray, $params);
    }

    public function getHTML()
    {
        ob_start();
        $this->controller = \core\Core::getInstance()->controllerObject;
        extract($this->paramsArray);
        include($this->templateFilePath);
        $str = ob_get_contents();
        ob_end_clean();
        return $str;
    }

    public function display()
    {
        echo $this->getHTML();
    }
}
