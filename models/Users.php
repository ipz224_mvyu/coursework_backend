<?php

namespace models;

use core\Model;
use core\Core;

/**
 * @property int $id Унікальний ідентифікатор користувача
 * @property string $login Логін користувача
 * @property string $password Пароль користувача
 * @property string $email Пошта користувача
 * @property string $first_name Ім'я користувача
 * @property string $last_name Прізвище користувача
 * @property string $phone Телефон користувача
 * @property string $date_of_birth Дата народження користувача
 * @property string $street_and_number Вулиця і номер будинку користувача
 * @property string|null $additional_address_line1 Додаткова адреса користувача (лінія 1)
 * @property string|null $additional_address_line2 Додаткова адреса користувача (лінія 2)
 * @property string $zip_code Поштовий індекс користувача
 * @property string $city Місто користувача
 * @property string $country Країна користувача
 */

class Users extends Model
{

    public static $tableName = 'users';
    public static function findByLoginAndPassword($login, $password)
    {
        $user = self::findByLogin($login);
        $isPasswordCorrect = password_verify($password, $user->password);

        if ($user && $isPasswordCorrect) {
            return $user;
        }

        return null;
    }

    public static function GetCurrentUser()
    {
        $user = Core::getInstance()->session->get('user');
        if ($user) {
            return $user;
        }
        return null;
    }

    public static function GetCurrentUserId()
    {
        $user = Core::getInstance()->session->get('user');
        if ($user) {
            return $user->id;
        }
        return null;
    }

    public static function findByLogin($login)
    {
        $rows = self::findByCondition(['email' => $login]);
        if (!empty($rows)) {
            return $rows[0];
        } else {
            return null;
        }
    }

    public static function IsUserLogged()
    {
        return !empty(Core::getInstance()->session->get('user'));
    }

    public static function GetAccess()
    {
        if (self::IsUserLogged()) {
            $user = Core::getInstance()->session->get('user');
            return isset($user->access_level) ? $user->access_level : 0;
        }
        return 0;
    }

    public static function LoginUser($user)
    {
        Core::getInstance()->session->set('user', $user);
    }

    public static function LogoutUser()
    {
        Core::getInstance()->session->remove('user');
    }

    public static function RegisterUser(
        $login,
        $email,
        $password,
        $first_name,
        $last_name,
        $phone,
        $date_of_birth,
        $street_and_number,
        $zip_code,
        $city,
        $country
    ) {
        $user = new Users();
        $user->login = $login;
        $user->email = $email;
        $user->password = $password;
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->phone = $phone;
        $user->date_of_birth = $date_of_birth;
        $user->street_and_number = $street_and_number;
        $user->zip_code = $zip_code;
        $user->city = $city;
        $user->country = $country;
        $user->save();
    }
}
