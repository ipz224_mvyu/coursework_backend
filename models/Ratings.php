<?php

namespace models;

use core\Core;
use core\Model;

/**
 * @property int $id Унікальний ідентифікатор рейтингу
 * @property int $id_product Унікальний ідентифікатор товару
 * @property int $rating рейтинг
 * @property int $id_user Унікальний ідентифікатор юзера
 */


class Ratings extends Model
{
    public static $tableName = 'product_ratings';

    public static function findByProductAndUser($productId, $id_user)
    {
        $rows = self::findByCondition(['id_product' => $productId, 'id_user' => $id_user]);
        if (!empty($rows)) {
            $rating = new self();
            $rating->load($rows[0]);
            return $rating;
        } else {
            return null;
        }
    }

    public static function findByProduct($productId)
    {
        $rows = self::findByCondition(['id_product' => $productId]);
        return $rows ?: [];
    }

    public static function updateRating($id, $data)
    {
        Core::getInstance()->db->update(
            static::$tableName,
            $data,
            ['id' => $id]
        );
    }

    public static function createRating($productId, $ratingValue, $userId)
    {
        $newRating = new Ratings();
        $newRating->id_product = $productId;
        $newRating->rating = $ratingValue;
        $newRating->id_user = $userId;
        $newRating->save();
    }

    public function load($data)
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }
}
