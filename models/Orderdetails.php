<?php

namespace models;

use core\Model;

/**
 * @property int $id Унікальний ідентифікатор деталей про замовлення
 * @property string $order_id  Ідентифікатор замовлення
 * @property string $product_id Ідентифікатор продукту
 * @property string $quantity Кількість товару
 * @property string $subtotal Ціна товару за кількістю
 */


class Orderdetails extends Model
{
    public static $tableName = 'orderdetails';

    public static function CreateOrderDetail($orderId, $productId, $quantity, $subtotal)
    {
        $orderdetails = new Orderdetails();
        $orderdetails->order_id  = $orderId;
        $orderdetails->product_id = $productId;
        $orderdetails->quantity = $quantity;
        $orderdetails->subtotal = $subtotal;

        $orderdetails->save();
    }

    public static function findByOrder($orderId)
    {
        $rows = self::findByCondition(['order_id' => $orderId]);
        if (!empty($rows)) {
            return $rows;
        } else {
            return null;
        }
    }

    public static function findOne($id)
    {
        $result = self::findById($id);
        if (!empty($result)) {
            return $result;
        }
        return null;
    }


    public static function deleteOrderDetail($orderDetailId)
    {
        $orderDetail = self::findById($orderDetailId);

        if ($orderDetail) {
            Orderdetails::deleteById($orderDetailId);
            return true;
        }

        return false;
    }

    public static function deleteOrderDetailByCondition($orderId)
    {
        $orderDetail = self::findByCondition(['order_id' => $orderId]);

        if (!empty($orderDetail)) {
            Orderdetails::deleteByCondition(['order_id' => $orderId]);
            return true;
        }
        return false;
    }
}
