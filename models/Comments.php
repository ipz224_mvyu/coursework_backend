<?php

namespace models;

use core\Model;

/**
 * @property int $id Унікальний ідентифікатор коментаря
 * @property int $user_id  Ідентифікатор користувача
 * @property int $product_id Ідентифікатор продукту
 * @property string $date Дата коментаря
 * @property string $description Опис коментаря
 * @property string $rating рейтинг
 * @property string $email email юзера
 */


class Comments extends Model
{
    public static $tableName = 'comments';

    public static function createComment($user_id, $product_id, $description, $rating, $email)
    {
        $comment = new Comments();
        $comment->user_id = $user_id;
        $comment->product_id = $product_id;
        $comment->date = date('Y-m-d H:i:s');
        $comment->description = $description;
        $comment->rating = $rating;
        $comment->email = $email;

        $comment->save();
    }

    public static function findByProduct($productId)
    {
        $rows = self::findByCondition(['product_id' => $productId]);
        if (!empty($rows)) {
            return $rows;
        } else {
            return null;
        }
    }

    public static function deleteComment($id)
    {
        $comment = self::findById($id);

        if (!empty($comment)) {
            Comments::deleteById($id);
            return true;
        }
        return false;
    }
}
