<?php

namespace models;

use core\Model;
use core\Core;

/**
 * @property int $order_id Унікальний ідентифікатор замовлення
 * @property string $user_phone Номер телефону користувача
 * @property string $order_date Дата замовлення
 * @property string $total_price Ціна замовлення
 * @property string $user_street_and_number	Вулиця і дім авторизованого користувача
 * @property string $user_city Місто авторизованог окористувача
 * @property string $unique_id Унікальний номер товару 
 */


class Orders extends Model
{
    public static $tableName = 'orders';

    public static function GetByUniq($uniq)
    {
        $rows = self::findByCondition(['unique_id' => $uniq]);
        if (!empty($rows)) {
            return $rows[0];
        } else {
            return null;
        }
    }

    public static function deleteOrder($id)
    {
        $order = self::findById($id);

        if (!empty($order)) {
            Orders::deleteById($id);
            return true;
        }
        return false;
    }

    public static function CreateOrder($phone, $total_price) 
    {
        $order = new Orders();
        $order->user_phone = $phone;
        $order->order_date = date('Y-m-d H:i:s');
        $order->total_price = $total_price;
        $order->unique_id = self::generateUniqueId(13);

        if (Users::IsUserLogged()) {
            $user = Users::getCurrentUser();
            $order->user_city = $user->city;
            $order->user_street_and_number = $user->street_and_number;
        }

        $order->save();
        return $order->unique_id;
    }

    public static function updateStatus($id, $data)
    {
        Core::getInstance()->db->update(
            static::$tableName,
            $data,
            ['id' => $id]
        );
    }

    private static function generateUniqueId($length = 13)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
