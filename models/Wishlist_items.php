<?php

namespace models;

use core\Model;
use core\Core;

/**
 * @property int $id Унікальний ідентифікатор списку
 * @property int $product_id Унікальний ідентифікатор продукту
 * @property int $wishlist_id Унікальний ідентифікатор списку продуктів
 */

class Wishlist_items extends Model
{
    public static $tableName = 'wishlist_items';

    public static function AddToWishlist($wishlistId, $productId)
    {
        $wishlist_item = new Wishlist_items();
        $wishlist_item->product_id = $productId;
        $wishlist_item->wishlist_id = $wishlistId;
        $wishlist_item->save();
    }

    public static function IsProductInWishlist($wishlist_id, $product_id)
    {
        $rows = self::findByCondition(['wishlist_id' => $wishlist_id, 'product_id' => $product_id]);
        if (!empty($rows)) {
            return $rows[0];
        } else {
            return null;
        }
    }

    public static function FindAllByWishlistId($wishlistId)
    {
        $rows = self::findByCondition(['wishlist_id' => $wishlistId]);
        if (!empty($rows)) {
            return $rows;
        } else {
            return null;
        }
    }

    public static function DeleteProduct($id)
    {
        Core::getInstance()->db->delete(static::$tableName, [static::$primaryKey => $id]);
    }
}
