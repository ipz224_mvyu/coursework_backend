<?php

namespace models;

use core\Core;
use core\Model;

/**
 * @property int $id Унікальний ідентифікатор товару
 * @property string $name Назва товару
 * @property string $description Опис товару
 * @property string $category Категорія товару
 * @property string $subcategory Підкатегорія товару
 * @property string $brand Бренд товару
 * @property float $price Ціна товару
 * @property int $discount Знижка на товар
 * @property int $stock_quantity Кількість товару на складі
 * @property int $color Фото товару
 * @property int $image Фото товару
 */


class Products extends Model
{

    public static $tableName = 'products';

    public static function getAllProducts()
    {
        $rows = self::findAll();
        if (!empty($rows)) {
            return $rows;
        } else {
            return null;
        }
    }

    public static function getAllProductsSerch($conditionAssocArray)
    {
        $rows = self::findBySearch($conditionAssocArray);
        if (!empty($rows)) {
            return $rows;
        } else {
            return null;
        }
    }

    public static function findOne($id)
    {
        $result = self::findById($id);
        if (!empty($result)) {
            return $result;
        }
        return null;
    }

    public static function updateStockQuantity($productId, $data)
    {
        Core::getInstance()->db->update(
            static::$tableName,
            $data,
            ['id' => $productId]
        );
    }

    public static function updateProduct($id, $data)
    {
        Core::getInstance()->db->update(
            static::$tableName,
            $data,
            ['id' => $id]
        );
    }

    public static function AddProduct(
        $name,
        $description,
        $category,
        $subcategory,
        $brand,
        $price,
        $discount,
        $stock_quantity,
        $color,
        $imagePath
    ) {
        $product = new Products();
        $product->name = $name;
        $product->description = $description;
        $product->category = $category;
        $product->subcategory = $subcategory;
        $product->brand = $brand;
        $product->price = $price;
        $product->discount = $discount;
        $product->stock_quantity = $stock_quantity;
        $product->color = $color;
        $product->image = $imagePath;
        $product->save();
    }

    public static function deleteProduct($id)
    {
        $product = self::findById($id);

        if (!empty($product)) {
            Products::deleteById($id);
            return true;
        }
        return false;
    }
}
