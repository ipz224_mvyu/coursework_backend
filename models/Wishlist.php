<?php

namespace models;

use core\Model;
use core\Core;

/**
 * @property int $id Унікальний ідентифікатор списку
 * @property int $user_id Унікальний ідентифікатор користувача
 */

class Wishlist extends Model
{
    public static $tableName = 'wishlist';
    protected static $primaryKey = 'id';

    public static function AddNewWishlist($id_user) {
        $wishlist = new Wishlist();
        $wishlist->user_id = $id_user;
        $wishlist->save();
    }

    public static function FindByUserId($id_user)
    {
        $rows = self::findByCondition(['user_id' => $id_user]);
        if (!empty($rows)) {
            return $rows[0];
        } else {
            return null;
        }
    }
}
