<?php

namespace controllers;

use core\Controller;
use models\Wishlist;
use models\Wishlist_items;
use models\Users;
use models\Products;

class WishlistController extends Controller
{
    public function actionAddToWishlist()
    {
        header('Content-Type: application/json');

        $input = json_decode(file_get_contents('php://input'), true);

        $productId = $input['product_id'];

        if (!Users::IsUserLogged()) {
            echo json_encode(['status' => 'error', 'message' => 'Будь ласка, авторизуйтесь']);
            exit;
        }

        $userId = Users::GetCurrentUserId();
        if ($userId === null) {
            echo json_encode(['status' => 'error', 'message' => 'Ви повинні увійти, щоб додавати товари до списку бажань.']);
            exit;
        }

        $wishlist = Wishlist::FindByUserId($userId);

        if (!$wishlist) {
            $wishlist = new Wishlist();
            $wishlist->user_id = $userId;
            $wishlist->save();
        }
        $deleteItem = Wishlist_items::IsProductInWishlist($wishlist->id, $productId);
        if ($deleteItem) {
            Wishlist_items::DeleteProduct($deleteItem->id);
            echo json_encode(['status' => 'success', 'message' => 'Товар видалено зі списку бажань.']);
            exit;
        }

        Wishlist_items::AddToWishlist($wishlist->id, $productId);
        echo json_encode(['status' => 'success', 'message' => 'Товар додано до списку бажань.']);
        exit;
    }

    public function actionGetWishlistProducts()
    {
        if (!Users::IsUserLogged()) {
            return $this->render('views/errors/404.php');
        }

        $userId = Users::GetCurrentUserId();
        if ($userId === null) {
            return $this->render('views/errors/404.php');
        }

        $wishlist = Wishlist::FindByUserId($userId);
        if (!$wishlist) {
            return $this->render('views/errors/404.php');
        }

        $wishlistItems = Wishlist_items::FindAllByWishlistId($wishlist->id);
        if (empty($wishlistItems)) {
            return $this->render('views/errors/404.php');
        }

        $productIds = array_column($wishlistItems, 'product_id');

        $products = [];

        foreach ($productIds as $productId) {
            $product = Products::findById($productId);
            if ($product) {
                $products[] = $product;
            } else {
                return $this->render('views/errors/404.php');
            }
        }
        if (empty($products)) {
            return $this->render('views/wishlist/index.php', ['products' => []]); 
        } else {
            return$this->render('views/wishlist/index.php', ['products' => $products]);
        }
    }
}
