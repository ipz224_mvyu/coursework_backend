<?php

namespace controllers;

use core\Controller;
use models\Orders;
use models\Orderdetails;
use models\Products;
use models\Users;

class OrdersController extends Controller
{
    public function actionCreate()
    {
        header('Content-Type: application/json');

        $requestData = json_decode(file_get_contents('php://input'), true);
        $phone = $requestData['phone'] ?? '';
        $totalPrice = $requestData['total_price'] ?? 0;
        $orderDetails = $requestData['order_details'] ?? [];

        if (empty($phone) || $totalPrice <= 0 || empty($orderDetails)) {
            echo json_encode(['error' => 'Невірні дані для створення замовлення.']);
            exit;
        }
        $orderUniq = Orders::CreateOrder($phone, $totalPrice);

        $order = Orders::GetByUniq($orderUniq);
        if ($order) {
            foreach ($orderDetails as $detail) {
                $productId = $detail['product_id'];
                $quantity = $detail['quantity'];
                $subtotal = $detail['subtotal'];

                OrderDetails::CreateOrderDetail($order->id, $productId, $quantity, $subtotal);
            }

            echo json_encode(['message' => 'Замовлення створено успішно.', 'order_id' => $order->id]);
            exit;
        } else {
            echo json_encode(['error' => 'Помилка при створенні замовлення.']);
            exit;
        }
    }

    public function actionView()
    {
        if (!Users::IsUserLogged() && Users::GetAccess() !== 4)
        {
            return $this->render('views/errors/404.php');
        }
        $orders = Orders::findAll();
        return $this->render('views/orders/view.php', ['orders' => $orders]);
    }

    public function actionDetails($orderId)
    {
        if (!Users::IsUserLogged() && Users::GetAccess() !== 4) {
            return $this->render('views/errors/404.php');
        }

        $orderDetails = Orderdetails::findByOrder($orderId);
        if (!$orderDetails) {
            return $this->render('views/errors/404.php');
        }

        $order = Orders::findById($orderId);
        $products = [];
        foreach ($orderDetails as $detail) {
            $product = Products::findById($detail->product_id);
            if ($product) {
                $products[] = [
                    'product' => $product,
                    'quantity' => $detail->quantity,
                    'subtotal' => $detail->subtotal,
                    'orderId' => $orderId[0],
                    'order_detail_id' => $detail->id
                ];
            }
        }

        $status = $order ? $order->status : null;

        return $this->render('views/orders/details.php', [
            'products' => $products,
            'status' => $status,
        ]);
    }

    public function actionDeleteDetail()
    {
        error_log("Entered actionDeleteDetail");

        $orderDetailId = $_POST['orderDetailId'];
        $orderId = $_POST['order_id'];

        $success = Orderdetails::deleteOrderDetail($orderDetailId);

        $orderDetails = Orderdetails::findByOrder($orderId);

        if ($orderDetails == null) {
            $successOrder = Orders::deleteOrder($orderId);

            if ($successOrder) {
                echo json_encode(['message' => 'Замовлення успішно видалено.', 'redirect' => '/orders/view']);
                exit;
            } else {
                echo json_encode(['error' => 'Помилка при видаленні замовлення.']);
                exit;
            }
        }

        if ($success) {
            echo json_encode(['message' => 'Деталь замовлення успішно видалено.']);
            exit;
        } else {
            echo json_encode(['error' => 'Помилка при видаленні деталі замовлення.']);
            exit;
        }
    }

    public function actionCheckout()
    {
        header('Content-Type: application/json');

        if (!isset($_POST['order_id'])) {
            echo json_encode(['error' => 'Не передано параметр order_id']);
            exit;
        }

        $orderId = $_POST['order_id'];

        $orderDetails = Orderdetails::findByOrder($orderId);

        if (!$orderDetails) {
            echo json_encode(['error' => 'Немає деталей для цього замовлення.']);
            exit;
        }

        foreach ($orderDetails as $detail) {
            $product = Products::findById($detail->product_id);
            if ($product) {
                if ($detail->quantity <= $product->stock_quantity) {
                    $newStockQuantity = $product->stock_quantity - $detail->quantity;
                    $updatedData = [
                        'stock_quantity' => $newStockQuantity,
                    ];
                    Products::updateStockQuantity($product->id, $updatedData);
                } else {
                    echo json_encode(['error' => 'Недостатня кількість товару на складі для продукту ' . $product->name]);
                    exit;
                }
            } else {
                echo json_encode(['error' => 'Продукт не знайдено для ID ' . $detail->product_id]);
                exit;
            }
        }

        $updatedStatus = [
            'status' => true,
        ];

        Orders::updateStatus($orderId, $updatedStatus);

        echo json_encode(['message' => 'Замовлення успішно оформлено.']);
        exit;
    }

    public function actionDeleteOrders()
    {
        header('Content-Type: application/json');

        $orderId = isset($_POST['order_id']) ? $_POST['order_id'] : null;

        if (empty($orderId)) {
            echo json_encode(['error' => 'Помилка при передачі параметра order_id']);
            exit;
        }

        $orderDetailsDeleted = Orderdetails::deleteOrderDetailByCondition($orderId);
        if (!$orderDetailsDeleted) {
            echo json_encode(['error' => 'Помилка! очистити деталі замовлення']);
            exit;
        }

        $orderDeleted = Orders::deleteOrder($orderId);
        if (!$orderDeleted) {
            echo json_encode(['error' => 'Помилка! видалити замовлення']);
            exit;
        }

        echo json_encode(['message' => 'Замовлення успішно видалено.']);
        exit;
    }
}
