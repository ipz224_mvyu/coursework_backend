<?php

namespace controllers;

use core\Controller;
use core\Template;
use models\Products;

class HomeController extends Controller
{
    public function actionIndex()
    {
        $allProducts = Products::getAllProducts();

        if ($allProducts) {
            $filteredProducts = array_filter($allProducts, function ($product) {
                return $product->stock_quantity > 0;
            });


            $productsWithDiscountPercentage = array_map(function ($product) {
                $discountAmount = $product->price * ($product->discount / 100);
                $discountedPrice = $product->price - $discountAmount;
                return [
                    'product' => $product,
                    'discountPercentage' => $discountAmount / $product->price * 100
                ];
            },
                $filteredProducts
            );

            usort($productsWithDiscountPercentage, function ($a, $b) {
                return $b['discountPercentage'] - $a['discountPercentage'];
            });

            $topDiscountedProducts = array_map(function ($item) {
                return $item['product'];
            },
                array_slice($productsWithDiscountPercentage, 0, 6)
            );

            return $this->render('views/home/view.php', ['products' => $topDiscountedProducts]);
        }
        return $this->render('views/home/view.php', ['products' => []]);
    }

    public function actionView($params)
    {
        return $this->render();
    }
}
