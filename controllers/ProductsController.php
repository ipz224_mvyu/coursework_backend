<?php

namespace controllers;

use core\Controller;
use models\Comments;
use models\Products;
use models\Users;
use models\Ratings;


class ProductsController extends Controller
{
    public function actionRatingUpdate()
    {
        header('Content-Type: application/json');

        if (Users::IsUserLogged()) {
            $user = Users::GetCurrentUser();
            $requestData = json_decode(file_get_contents('php://input'), true);

            if (!isset($requestData['product_id']) || !isset($requestData['rating'])) {
                echo json_encode(['error' => 'Invalid request']);
                exit;
            }
            $productId = $requestData['product_id'];
            $ratingValue = $requestData['rating'];

            error_log("User: {$user->id}, Product: $productId, Rating: $ratingValue");

            $existingRating = Ratings::findByProductAndUser($productId, $user->id);

            if ($existingRating) {
                $updatedData = [
                    'rating' => $ratingValue,
                ];
                Ratings::updateRating($existingRating->id, $updatedData);
            } else {
                $newRating = new Ratings();
                $newRating->id_product = $productId;
                $newRating->rating = $ratingValue;
                $newRating->id_user = $user->id;
                $newRating->save();
            }

            $ratingsByProduct = Ratings::findByProduct($productId);
            $totalRating = 0;
            $ratingsCount = count($ratingsByProduct);
            foreach ($ratingsByProduct as $rating) {
                $totalRating += $rating->rating;
            }

            $averageRating = ($ratingsCount > 0) ? $totalRating / $ratingsCount : 0;

            echo json_encode(['average_rating' => round($averageRating, 2)]);
            exit;
        } else {
            echo json_encode(['error' => 'Please log in']);
            exit;
        }
    }

    public function actionAddComment()
    {
        header('Content-Type: application/json');

        if (Users::IsUserLogged()) {
            $user = Users::GetCurrentUser();
            $requestData = json_decode(file_get_contents('php://input'), true);

            if (!isset($requestData['product_id']) || !isset($requestData['description'])) {
                echo json_encode(['error' => 'Invalid request']);
                exit;
            }

            $productId = $requestData['product_id'];
            $description = $requestData['description'];
            $ratingValue = Ratings::findByProductAndUser($productId, $user->id);

            Comments::createComment($user->id, $productId, $description, $ratingValue->rating, $user->email);

            $comments = Comments::findByProduct($productId);
            $commentsData = [];

            foreach ($comments as $comment) {
                $commentsData[] = [
                    'user_email' => $user->email,
                    'description' => $comment->description,
                    'rating' => $comment->rating,
                    'created_at' => $comment->date,
                ];
            }

            echo json_encode(['success' => true, 'comments' => $commentsData]);
            exit;
        } else {
            echo json_encode(['error' => 'Please log in']);
            exit;
        }
    }

    public function actionDeleteComment()
    {
        header('Content-Type: application/json');

        if (Users::IsUserLogged()) {
            $user = Users::GetCurrentUser();
            $requestData = json_decode(file_get_contents('php://input'), true);

            if (!isset($requestData['comment_id']) || !isset($requestData['product_id'])) {
                echo json_encode(['error' => 'Invalid request']);
                exit;
            }

            $commentId = $requestData['comment_id'];
            $productId = $requestData['product_id'];

            $comment = Comments::findById($commentId);

            if ($comment && $comment->user_id === $user->id) {
                Comments::deleteComment($commentId);

                $comments = Comments::findByProduct($productId);
                $commentsData = [];

                if ($comments) {
                    foreach ($comments as $comment) {
                        $commentsData[] = [
                            'id' => $comment->id,
                            'user_email' => $comment->user_email,
                            'description' => $comment->description,
                            'rating' => $comment->rating,
                            'created_at' => $comment->date,
                        ];
                    }
                }

                echo json_encode(['success' => true, 'comments' => $commentsData]);
                exit;
            } else {
                echo json_encode(['error' => 'Comment not found or unauthorized']);
                exit;
            }
        } else {
            echo json_encode(['error' => 'Please log in']);
            exit;
        }
    }

    public function actionUpdateDiscount()
    {
        header('Content-Type: application/json');

        $requestData = json_decode(file_get_contents('php://input'), true);

        if (!isset($requestData['product_id']) || !isset($requestData['discount'])) {
            echo json_encode(['error' => 'Invalid request']);
            exit;
        }
        $productId = $requestData['product_id'];
        $newDiscount = $requestData['discount'];

        $product = Products::findById($productId);
        if ($product) {
            $updatedData = [
                'discount' => $newDiscount,
            ];
            Products::updateProduct($product->id, $updatedData);
            echo json_encode(['success' => 'Discount updated successfully']);
        } else {
            echo json_encode(['error' => 'Product not found']);
        }
        exit;
    }

    public function actionUpdatePrice()
    {
        header('Content-Type: application/json');

        $requestData = json_decode(file_get_contents('php://input'), true);

        if (!isset($requestData['product_id']) || !isset($requestData['price'])) {
            echo json_encode(['error' => 'Invalid request']);
            exit;
        }
        $productId = $requestData['product_id'];
        $newPrice = $requestData['price'];

        $product = Products::findById($productId);
        if ($product) {
            $updatedData = [
                'price' => $newPrice,
            ];
            Products::updateProduct($product->id, $updatedData);
            echo json_encode(['success' => 'Price updated successfully']);
        } else {
            echo json_encode(['error' => 'Product not found']);
        }
        exit;
    }

    public function actionIndex()
    {
        $products = Products::findAll();
        return $this->render('views/products/index.php', ['products' => $products]);
    }

    public function actionShow($productId)
    {
        $product = Products::findOne($productId);
        if (!$product) {
            return $this->render('views/errors/404.php');
        }

        $ratingsByProduct = Ratings::findByProduct($productId);

        $totalRating = 0;
        $ratingsCount = count($ratingsByProduct);

        foreach ($ratingsByProduct as $rating) {
            $totalRating += $rating->rating;
        }

        $averageRating = ($ratingsCount > 0) ? $totalRating / $ratingsCount : 0;

        $comments = Comments::findByProduct($productId);

        return $this->render('views/products/show.php', [
            'product' => $product,
            'averageRating' => round($averageRating, 2),
            'ratings' => $ratingsByProduct,
            'comments' => $comments,
        ]);
    }

    public function actionSearch($input)
    {
        if (isset($_POST['submit'])) {
            $input = $_POST['input'];
        }
        $products = Products::findBySearch($input);
        if ($products === null) {
            return $this->render('views/errors/404.php');
        }

        return $this->render('views/products/index.php', ['products' => $products]);
    }

    public function actionCategory($subcategory)
    {
        $current_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $parts = parse_url($current_url);
        $category = null;

        if (isset($parts['path'])) {
            $path_parts = explode('/', $parts['path']);
            $category = end($path_parts);
            $category = urldecode($category);
        }

        if (empty($category) || $category == 'category') {
            return $this->redirect('/views/errors/404.php');
        }

        $products = Products::findByCondition(['subcategory' => $subcategory]);
        return $this->render('views/products/subcategory.php', ['products' => $products]);
    }

    public function actionMaincategory($category)
    {
        $products = Products::findByCondition(['category' => $category]);
        return $this->render('views/products/category.php', ['products' => $products]);
    }

    public function actionGet()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $ids = explode(';', $data['ids']);

        $products = [];

        foreach ($ids as $id) {
            $product = Products::findOne($id);
            if ($product) {
                $products[] = $product;
            }
        }

        header('Content-Type: application/json');
        echo json_encode($products);
        exit; 
    }

    public function actionAddProduct()
    {
        if ($this->isPost) {
            if (
                strlen($this->post->name) === 0 ||
                strlen($this->post->description) === 0 ||
                strlen($this->post->category) === 0 ||
                strlen($this->post->subcategory) === 0 ||
                strlen($this->post->brand) === 0 ||
                strlen($this->post->price) === 0 ||
                strlen($this->post->stock_quantity) === 0
            ) {
                $this->addErrorMessage('Потрібно заповнити всі обов’язкові поля!!!');
            }
            if (isset($this->post->discount)) {
                $discount = (float)$this->post->discount;
                if ($discount < 0 || $discount > 100) {
                    $this->addErrorMessage('Знижка повинна бути між 0 та 100 відсотками');
                }
            } else {
                $discount = null;
            }

            $product = Products::findByCondition(['name' => $this->post->name]);
            if($product){
                $this->addErrorMessage('Такий товар вже існує!!');
            }

            if (!$this->isErrorMessageExist()) {
                $imagePath = $this->uploadImage($_FILES['image']);

                Products::AddProduct(
                    $this->post->name,
                    $this->post->description,
                    $this->post->category,
                    $this->post->subcategory,
                    $this->post->brand,
                    $this->post->price,
                    $this->post->discount,
                    $this->post->stock_quantity,
                    $this->post->color,
                    basename($imagePath)
                );
            }
        }
        return $this->render('views/products/addform.php');
    }

    public function actionDeleteProduct()
    {
        $productId = intval($this->post->product_id);
        if(!empty($productId))
        {
            $status = Products::deleteProduct($productId);
            if($status){
                return $this->redirect('/');
            }
        }
    }

    private function uploadImage($imageFile)
    {
        $targetDir = "D:/My_/Institut/WAMP/domains/cms/src/img/";
        $targetFile = $targetDir . basename($imageFile["name"]);
        if (move_uploaded_file($imageFile["tmp_name"], $targetFile)) {
            return $targetFile;
        } else {
            $this->addErrorMessage('Помилка при завантаженні зображення');
            return null;
        }
    }
}
