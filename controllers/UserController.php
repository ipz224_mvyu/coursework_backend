<?php

namespace controllers;

use models\Users;
use models\Wishlist;

class UserController extends \core\Controller
{
    public function actionRegister()
    {
        if ($this->isPost) {
            $user = Users::findByLogin($this->post->login);
            if (!empty($user)) {
                $this->addErrorMessage('Користувач із таким логіном вже існує!!!');
            }
            if ($this->post->password != $this->post->confirm_password) {
                $this->addErrorMessage('Паролі не співпадають');
            }
            if (
                strlen($this->post->login) === 0 ||
                strlen($this->post->email) === 0 ||
                strlen($this->post->password) === 0 ||
                strlen($this->post->first_name) === 0 ||
                strlen($this->post->last_name) === 0 ||
                strlen($this->post->phone) === 0 ||
                strlen($this->post->date_of_birth) === 0 ||
                strlen($this->post->street_and_number) === 0 ||
                strlen($this->post->zip_code) === 0 ||
                strlen($this->post->city) === 0 ||
                strlen($this->post->country) === 0
            ) {
                $this->addErrorMessage('Потрібно заповнити всі поля!!!');
            }
            if (!$this->isErrorMEssageExist()) {

                $hashedPassword = password_hash($this->post->password, PASSWORD_DEFAULT);

                Users::RegisterUser(
                    $this->post->login,
                    $this->post->email,
                    $hashedPassword,
                    $this->post->first_name,
                    $this->post->last_name,
                    $this->post->phone,
                    $this->post->date_of_birth,
                    $this->post->street_and_number,
                    $this->post->zip_code,
                    $this->post->city,
                    $this->post->country
                );

                $user = Users::findByLoginAndPassword($this->post->email, $this->post->password);
                if ($user) {
                    Wishlist::AddNewWishlist($user->id);
                    return $this->redirect('/');
                } else {
                    $this->addErrorMessage('Помилка при реєстрації користувача');
                }
            }
        }
        return $this->render();
    }

    public function actionLogin()
    {
        if (Users::IsUserLogged()) {
            return $this->redirect('/');
        }
        if ($this->isPost) {
            $user = Users::findByLogin($this->post->login);
            if ($user && password_verify($this->post->password, $user->password)) {
                Users::LoginUser($user);
                return $this->redirect('/');
            } else {
                $this->addErrorMessage('Неправильний логін або пароль!!!');
            }
        }

        return $this->render();
    }

    public function actionLogout()
    {
        Users::LogoutUser();
        return $this->redirect('/user/login');
    }

    public function actionProfile()
    {

    }
}
